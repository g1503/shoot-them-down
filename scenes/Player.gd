extends Area2D

enum States {idle,up,down,dead} 

signal player_down

onready var hull = $Hull
onready var laser = $LaserBeam
onready var animation = $AnimationPlayer

var state = States.idle
var immune = false
var speed = 300

export (float) var max_height = 0
export (float) var min_height = 0


func _process(delta):

	if state != 3 and Globals.charge <= 0:
		state = States.dead
		emit_signal("player_down")
	
	if state != 3: listen_controls()
	
	if state == 1 and position.y > max_height:
		position.y -= speed * delta
		hull.animation = "up"
	elif state == 2 and position.y < min_height:
		position.y += speed * delta
		hull.animation = "down"
	elif state == 0:
		hull.animation = "idle"
	
	if Globals.charge > 0:
		Globals.charge -= Globals.charge_leak * delta
		get_parent().charge_bar.value = Globals.charge
	else:
		Globals.charge = 0


func listen_controls():
	
	if Input.is_action_just_pressed("fire_weapon") and !laser.is_fired:
		laser.is_fired = true

	if Input.is_action_just_released("fire_weapon") and laser.is_fired:
		laser.is_fired = false

	if Input.is_action_pressed("up") and !Input.is_action_pressed("down"):
		state = States.up
	
	if Input.is_action_pressed("down") and !Input.is_action_pressed("up"):
		state = States.down

	if Input.is_action_just_released("down") or Input.is_action_just_released("up"):
		state = States.idle


func hit(_attacker):
	
	if !immune and Globals.health > 0:
		Globals.health -= 1
		get_parent().health_label.text = ": %s"%Globals.health
		if Globals.health <= 0:
			Globals.health = 0
			state = States.dead
			emit_signal("player_down")
		else:
			immune = true
			animation.play("Immune")


func on_immune_expired():
	
	immune = false


func spawn_cell(vector):
	
	var cell = preload("res://scenes/Cell.tscn").instance()
	add_child(cell)
	cell.global_position = vector

func _on_Player_area_entered(area):
	
	if area.name == "Cell":
		area.consume()
