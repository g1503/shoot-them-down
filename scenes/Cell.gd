extends Area2D


func _physics_process(delta):
	
	if position.x > get_parent().position.x:
		position.x -= Globals.move_speed*20 * delta
	if position.x < get_parent().position.x:
		consume()
	
	

func consume():
	if Globals.charge < Globals.max_charge:
		Globals.charge += 10
	queue_free()


func _on_CellLifespan_timeout():
	
	consume()
	
