extends Node

var sound = true

var health = 3

var charge = 100
var max_charge = 110

var default_move_speed = 150
var move_speed = 150

var default_charge_leak = 1
var charge_leak = 1


const endgame_messages = [
	"""You managed to survive this crash?! Great!\n
	But you need to be strong for what comes next...\n
	Brace yourself. Aliens captured planet and put pineaples\n
	in every pizza in the world!\n
	We were not prepared to such a cruelty...""",
	"""It's a shame that they shot you down, but don't be upset.\n
	Our best minds in the world already working on something new.\n
	Right now they mounting Patriot missiles on AN-2!\n
	Victory will be ours!!!""",
	"""You are no match for us, Earthlings! Soon your planet become a\n
	beautiful farm for our intelligent tofu! Ak-ak-ak-ak-ak!!!""",
	"""This is weird. Aliens left the Earth right after your defeat.\n
	Apperentally they hated this plane design so much\n
	to initiate whole scaled invasion just to destroy it...""",
	"""Oh you survived? That's, ummm... great.\n
	Listen, turned out that this whole invasion was just brand new\n
	Chinese drones hacked by some random Ukrainian hacker.\n
	Now China demands compensation for so many of them destroyed.\n
	It seems our salries will be cutted for years..."""
]
