extends ParallaxLayer

export (float) var speed = 100.0


func _physics_process(delta):
	
	motion_offset.x -= speed * delta
