This project was initially made for Triple Trijam game jam on Itch.io https://itch.io/jam/triple-trijam-2020

Plane and UFOs are sprites generated from 3D models in Kenney's Assets Forge

Game developed in Godot engine.

Main goal of player is too shoot down UFOs while mainaitning BFG charge above 0.